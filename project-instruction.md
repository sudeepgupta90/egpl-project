## Economics data slackbot

We would like to build a simple slackbot for the economics data, where
we can ask questions like

```
nextevent
nextevent 20190905
nextevent Japan
```

That posts economics events for today, a certain date, or a certain
country.

At the end of the day, it also posts a summary of events for the next
business day.

### Economics events fetcher

To begin, you need to write code to fetch the economic event calendar
for a date from `https://calendar.fxstreet.com`.

For instance, to get the events for 20190902, one would use the
following query URL

```
https://calendar.fxstreet.com/eventdate/?f=csv&v=2&timezone=UTC&view=range&start=20190901&end=20190902
```

Sending this query alone would not work because you need to set the
`referer` in the header to be
`https://www.fxstreet.com/economic-calendar`.

It is best to return a Pandas DataFrame from this function.

### Slack client

To post the economics data on slack, you need to write a slack
client. You can follow the many instructions online to set up a
bot,
e.g. `https://github.com/slackapi/python-slackclient/tree/master/tutorial`.

### Deliverables

- readme.md that explains the design of different components and the
  requirements to run the code.

- Python module `econ_events` that fetches the data from different
  data sources.

- Python module `econbot` with an `app.py` script that is the slackbot.

### Evaluation

The code is evaluated for its

1. Style,
2. Robustness,
3. Exntensibility,
4. Correctness.

Your communication with us and how you tackle the problem is evaluated
as well.

Please make sure to clarify any requirement or step with me when these
instructions are not clear.

### Future improvements

- For `fxstreet`, you can improve the query to only fetch a country's
  data.

- To be more accurate, we would like to get data from different
  source, e.g., `https://talking-forex.com/`. Can the event fetcher
  and the bot you wrote be extended to do that. Please discuss what
  is required to do that.


