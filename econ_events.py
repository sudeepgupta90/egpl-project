import os
import requests
import pandas as pd
from abc import ABC, abstractmethod
import time

class EconomyCalendarEventSource(ABC):

    def hello(self):
        return "Hello World!"
    
    @abstractmethod
    def day(self, date):
        pass
    
    @abstractmethod
    def date_range(self, startDate, endDate):
        pass
    
    @abstractmethod
    def country_day(self, countryCode, eventDate):
        pass
    

class FXStreet(EconomyCalendarEventSource):
    
    _countryCodeList= {"AR","AU","AT","BE","BR","CA","CL","CN","CO","CZ","DK","EMU","FI","FR","DE","GR","HK","HU","IS","IN","ID","IE","IT","JP","MX","NL","NZ","NO","PL","PT","RO","RU","SG","SK","ZA","ES","SE","CH","TR","UK","US"}

    def today(self):
        now= time.strftime("%Y%m%d")
        return self.day(now)

    def day(self, date):
        #TODO check for date validity here vs defer to API
        return self.date_range(date, date)
    
    def date_range(self, startDate, endDate=""):
        if endDate=="":
            endDate=startDate
        url= "https://calendar.fxstreet.com/eventdate/?f=csv&v=2&timezone=UTC&view=range&start={}&end={}".format(startDate, endDate)

        return self.fetchData(url)

    def country_day(self, countryCode, eventDate=""):
        if countryCode not in self._countryCodeList:
            raise KeyError("Please specify a valid country code")
        else:
            if eventDate=="":
                eventDate=time.strftime("%Y%m%d")
            url= "https://calendar.fxstreet.com/eventdate/?f=csv&v=2&timezone=UTC&view=range&start={}&end={}&countrycode={}".format(eventDate, eventDate, countryCode)
            return self.fetchData(url)
    
    
    def fetchData(self, url):
        referer= "https://www.fxstreet.com/economic-calendar"
        headers = {
            "Referer": referer
            }

        r= requests.get(url, headers= headers)

        if r.text is not None:
            return self.transformResponse(r.text)
        else:
            raise Exception("No data found")
    
    def transformResponse(self, response):
        if response:
            data= response.split("\r\n")
            columns, response_data= data[0].split(","), data[1:]
            column_len= len(columns)
            response_data= [x.split(",") if len(x.split(","))==column_len else x.split(",")[:column_len] for x in response_data]    #//TODO correct this later
            
            df= pd.DataFrame(response_data, columns= columns)
            
            return df

