from datetime import datetime, timedelta

def validDate(string):
    flag= False
    try:
        datetime.strptime(string, "%Y%m%d")
        flag= True
    except:
        pass

    return flag

def tomorrow():
    d= datetime.today() + timedelta(days=1)
    tomorrow= d.strftime("%Y%m%d")
    
    return tomorrow
