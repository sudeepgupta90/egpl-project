import os
import time
import slack
import ssl as ssl_lib
import certifi

import pandas as pd
from tabulate import tabulate

from econ_events import FXStreet
from botutil import validDate, tomorrow

import logging

bot= FXStreet()

def nextevent(text):
    result=""
    query= text.split(" ")
    query_len= len(query)

    
    if query_len==1:
        result= bot.today() #nextevent
        # result= bot.hello()   # dev test call
    elif query_len==2:
        if validDate(query[1]):     
            result= bot.day(query[1])       # nextevent date
        else:
            result= bot.country_day(query[1])

    if isinstance(result, pd.DataFrame):
        result= tabulate(result, headers='keys', tablefmt='psql')
    
    return result


@slack.RTMClient.run_on(event="message")
def message(**payload):
    data = payload["data"]
    web_client = payload["web_client"]
    channel_id = data.get("channel")
    # user_id = data.get("user")
    text = data.get("text")

    # print (
    #     "data:", data, "\n\n",
    #     "client", web_client,"\n\n",
    #     "ch", channel_id,"\n\n",
    #     # "user", user_id,"\n\n",
    #     "t", text
    # )

    result=""
    if text.startswith("nextevent"):
        result= nextevent(text)
    if text.startswith("Reminder"): #"Reminder: nextdayevent."
        x= text.split(" ")[-1][:-1] # need to discard the period attached to the end of the string
        if x== "nextdayevent":
            s= "nextevent"+" "+ tomorrow()
            result=nextevent(s)
        
    response= web_client.chat_postMessage(channel= channel_id, text=result )
    assert response["ok"]


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    
    slack_token= os.environ["SLACK_BOT_TOKEN"]
    ssl_context = ssl_lib.create_default_context(cafile=certifi.where())
    
    rtm_client= slack.RTMClient(token=slack_token, ssl= ssl_context)
    rtm_client.start()
