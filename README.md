# econbot
---

The econbot has the following API calls:

    - nextevent
    - nextevent date where date is YYYYMMDD format
    - nextevent countryCode
    - remind/repeat calls setup (described in another section below)

The list of country codes is as follows:

    - AR
    - AU
    - AT
    - BE
    - BR
    - CA
    - CL
    - CN
    - CO
    - CZ
    - DK
    - EMU
    - FI
    - FR
    - DE
    - GR
    - HK
    - HU
    - IS
    - IN
    - ID
    - IE
    - IT
    - JP
    - MX
    - NL
    - NZ
    - NO
    - PL
    - PT
    - RO
    - RU
    - SG
    - SK
    - ZA
    - ES
    - SE
    - CH
    - TR
    - UK
    - US

## Understanding the App Architecture and Extending It

The Data Source is modelled through an Abstract Base Class `EconomyCalendarEventSource` with `abstract methods` for:

    - day
    - date_range
    - country_day

These methods can be extended to return data for any class which inherits from the base class.

For example, see implmentation of `FXStreet`

To extendfor another data source, at most event/command names need to be changed- assuming that all data bots will be hosted simultaenously. Otherwise, say a bot per channel- this will work as is.

In `app.py` set bot to another data source class. 

    bot= FXStreet()

### remind/repeat calls

**To setup the reminder call at the end of every business day. Use the following:**

    /remind #channel "nextdayevent" at 21:00 every day
    
    for example: /remind #test "nextdayevent" at 21:00 every day

*This will post the summary of events for the following day, every day at 21:00 hours.*

## Environment Setup

To setup the bot. Join the slack channel(invite already shared on email). The bot needs to be on the channel you will be testing on. Create a new channel, or join an existing one, and add the bot it, if not already.

Clone this repository and setup the python environment.

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt

Export auth tokens

    source setup

Start the bot, and post any API message as discussed already:

    python app.py
